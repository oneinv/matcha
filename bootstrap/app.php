<?php

session_start();
require __DIR__ . '/../vendor/autoload.php';

$app = new \Slim\App([
	'settings' => [
		'displayErrorDetails' => true,
		'db' => [
			'driver' => 'mysql',
			'host' => '127.0.0.1',
			'database' => 'test',
			'username' => 'root',
			'password' => '002191',
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => '',
		]
	]
]);

$container = $app->getContainer();

$container['auth'] = function($container) {
	return new \App\Auth\Auth();
};

$container['flash'] = function ($container) {
	return new \Slim\Flash\Messages;
};

$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
		'cache' => false,
		'debug' => true,
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container->router,
		$container->request->getUri()
	));
	$view->addExtension(new \Twig\Extension\DebugExtension());

	$view->getEnvironment()->addGlobal('user', [
		'check' => $container->auth->check(),
		'user' => $container->auth->user(),
	]);

	$view->getEnvironment()->addGlobal('flash', $container->flash);

	return $view;
};

$container['HomeController'] = function($container) {
	return new \App\Controllers\HomeController($container);
};

$container['BrowseController'] = function($container) {
	return new \App\Controllers\BrowseController($container);
};

$container['UserController'] = function($container) {
	return new \App\Controllers\UserController($container);
};

$container['SearchController'] = function($container) {
	return new \App\Controllers\SearchController($container);
};

$container['ChatController'] = function($container) {
	return new \App\Controllers\ChatController($container);
};

$container['FakeController'] = function($container) {
	return new \App\Controllers\FakeController($container);
};

$container['csrf'] = function ($container) {
	return new \Slim\Csrf\Guard;
};




$app->add(new \App\Middleware\ValidationErrorsMiddleware($container));
$app->add(new \App\Middleware\SaveInputMiddleware($container));
$app->add(new \App\Middleware\CsrfViewMiddleware($container));

$app->add($container->csrf);



require __DIR__ . '/../app/routes.php';
