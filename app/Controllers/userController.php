<?php

namespace App\Controllers;
require_once(ROOT. "/app/Components/db_generator.php");
use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Photo;
use App\Models\Location;
use App\Models\Tags;
use App\Models\Online;
use App\Models\Like;
use App\Models\Views;
use App\Auth\Auth;
use Exception;

class UserController extends Controller
{
	// logout user
	public function getSignOut($request, $response)
	{
		$this->container->auth->logout();
		return $response->withRedirect($this->container->router->pathFor('home'));
	}

	// login user
	public function getSignIn($request, $response)
	{
		return $this->container->view->render($response, '/user/signin.twig');
	}

	// complete login user with credits and redirect to home
	public function postSignIn($request, $response)
	{
		$info = $request->getParams();
		if (isset($info['forgot'])) {
			$email = $request->getParam('email');
			if (User::checkMailExists($email)) {
				$token = User::getToken($email);
				User::sendMail($email, $token, "recover");
				$this->container->flash->addMessage('info', 'Password resend to your email!');
				return $response->withRedirect($this->container->router->pathFor('home'));
			} else {
				$this->container->flash->addMessage('error', 'No such email!');
				return $response->withRedirect($this->container->router->pathFor('user.signin'));
			}
		} elseif (isset($info['g-recaptcha-response'])) {
			$url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";

			$secret_key = '6LdFjW8UAAAAAEySyJXDTiWhheKuKyj8tj2OjhFm';
			$query = $url_to_google_api . '?secret=' . $secret_key . '&response='
			. $_POST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
			$data = json_decode(file_get_contents($query));
			if ($data->success) {
				$auth = $this->container->auth->attempt(
					$request->getParam('email'),
					$request->getParam('password')
				);
				// $arr = generate_db();
				// foreach ($arr as $value) {
				// 	User::fill_db($value);
				// }
				if (!$auth) {
					$this->container->flash->addMessage('error', 'Wrong data');
					return $response->withRedirect($this->container->router->pathFor('user.signin'));
				}

				if (User::isReady($_SESSION['user']) !== "okay") {
					$this->container->flash->addMessage('error', 'Fill all information fields');
					if (!Online::isAdded($_SESSION['user']))
						Online::AddOnline($_SESSION['user']);
					else
						Online::UpdateOnline($_SESSION['user']);
					return $response->withRedirect($this->container->router->pathFor('user.account'));
				} else {
					if (!Online::isAdded($_SESSION['user']))
						Online::AddOnline($_SESSION['user']);
					else
						Online::UpdateOnline($_SESSION['user']);
					return $response->withRedirect($this->container->router->pathFor('home'));
				}
			} else {
				$this->container->flash->addMessage('error', 'Please valid captcha!');
				return $response->withRedirect($this->container->router->pathFor('user.signin'));
			}
		}
	}

	// registration user
	public function getSignUp($request, $response)
	{
		return $this->container->view->render($response, '/user/signup.twig');
	}

	// complete registration with rules and redirect
	public function postSignUp($request, $response)
	{
		$errors = array();
		$data = array();
		$data = $request->getParams();
		$errors = $this->catchErrors($data);

		if (count($errors)) {
			$this->container->view->getEnvironment()->addGlobal('errors', $errors);
			return $this->container->view->render($response, '/user/signup.twig');
		}
		$user = $request->getParams();
		$pswd = $user['password'];
		$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
		User::register($user['login'], $user['email'], $user['password']);
		User::sendMail($user['email'], User::getToken($user['email']), "activate");
		// как бы работает автологин но нужно подумать что делать с
			// окном заполнения всех полей. так что пока отключен
		// $this->container->auth->attempt($user['email'], $pswd);
		$this->container->flash->addMessage('info', 'Succsessfuly registered!');
		return $response->withRedirect($this->container->router->pathFor('home'));
	}


	// rendering email changing
	public function getChangeEmail($request, $response)
	{
		return $this->container->view->render($response, '/user/mail.twig');
	}

	//complete changing email with validation
	public function postChangeEmail($request, $response)
	{
		$user = ($this->container->auth->user());
		$errors = array();
		$data = array();
		$data['email'] = $user['email'];
		$data['email_old'] = $request->getParam('email_old');
		$data['email_new'] = $request->getParam('email_new');
		$errors = $this->catchChange("email", $data);

		if (count($errors) !== 0) {
			$this->container->view->getEnvironment()->addGlobal('errors', $errors);
			return $this->container->view->render($response, '/user/mail.twig');
		} else {
			User::updateEmail($user['id'], $data['email_new']);
			$this->container->flash->addMessage('info', 'Your email succesfuly changed.');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}
	}

	// rendering password changing
	public function getChangePassword($request, $response)
	{
		return $this->container->view->render($response, '/user/password.twig');
	}

	//complete changing password with validation
	public function postChangePassword($request, $response)
	{
		$user = ($this->container->auth->user());
		$errors = array();
		$data = array();
		$data['password'] = $user['password'];
		$data['password_old'] = $request->getParam('password_old');
		$data['password_new'] = $request->getParam('password_new');
		$errors = $this->catchChange("password", $data);

		if (count($errors) !== 0) {
			$this->container->view->getEnvironment()->addGlobal('errors', $errors);
			return $this->container->view->render($response, '/user/password.twig');
		} else {
			User::updatePassword($user['id'], $data['password_new']);
			$this->container->flash->addMessage('info', 'Your password succesfuly changed.');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}
	}

	//rendering user data
	public function getChangeData($request, $response)
	{
		$user_data = User::getFullUserByID($_SESSION['user']);
		$loc = Location::getLoc();
		$tags = Tags::getTags();

		$this->container->view->getEnvironment()->addGlobal('tags', $tags);
		$this->container->view->getEnvironment()->addGlobal('loc', $loc);
		$this->container->view->getEnvironment()->addGlobal('user_data', $user_data);
		return $this->container->view->render($response, '/user/accountchange.twig');
	}

	public function postChangeData($request, $response)
	{
		$user = $request->getParams();
		if($user['country'] === "None" || $user['city'] === "None") {
			$user_ip = file_get_contents('https://api.ipify.org');
			$geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$user_ip));
			$user['geo'] = $geo['geoplugin_regionName'] . "/" . $geo['geoplugin_countryName'];
		} else {
			$user['geo'] = $user['city'] . "/" . $user['country'];
		}
		if ($user['tag_list']) {
			$user['tag_list'] = implode(",", $user['tag_list'] );
		} else {
			$user['tag_list'] = "php";
		}
		User::editData($_SESSION['user'], $user);
		Auth::updateList($_SESSION['user']);
		return $response->withRedirect($this->container->router->pathFor('home'));
	}

	public function getData($request, $response)
	{
		$user_data = User::getFullUserByID($_SESSION['user']);
		unset($user_data['password'], $user_data['token'], $user_data['is_auth']);

		$user_photo = Photo::getPhotosByUserId($user_data['id']);

		$tmp = Like::getAllLikesForUser($_SESSION['user']);
		$i = 0;
		foreach ($tmp as $value) {
			$likes[$i]['id'] = $value['user_id'];
			$tmp = User::getUserLoginByID($value['user_id']);
			$likes[$i]['login'] = $tmp['login'];
			$i++;
		}
		$tmp = array();
		$tmp = Views::getAllViewsForUser($_SESSION['user']);
		$i = 0;
		foreach ($tmp as $value) {
			$views[$i]['id'] = $value['user_id'];
			$tmp = User::getUserLoginByID($value['user_id']);
			$views[$i]['login'] = $tmp['login'];
			$i++;
		}
		$this->container->view->getEnvironment()->addGlobal('views', $views);
		$this->container->view->getEnvironment()->addGlobal('likes', $likes);
		$this->container->view->getEnvironment()->addGlobal('user_data', $user_data);
		$this->container->view->getEnvironment()->addGlobal('user_photo', $user_photo);
		return $this->container->view->render($response, '/user/account.twig');
	}

	public function getAva($request, $response, $args)
	{
		$id = $args['id'];
	 	Photo::updateAva($id, $_SESSION['user']);
		Photo::setAva($_SESSION['user']);
		$this->container->flash->addMessage('info', 'Avatar updated.');
		return $response->withRedirect($this->container->router->pathFor('user.account'));
	}

	public function delPhoto($request, $response, $args)
	{
		$id = $args['id'];
		if (User::isAva(Photo::getPhotoById($id), $_SESSION['user']))
		    User::updateAvaToDefault($_SESSION['user']);
		Photo::deletePhoto(Photo::getPhotoById($id),$_SESSION['user']);
		$this->container->flash->addMessage('info', 'Photo deleted.');
		return $response->withRedirect($this->container->router->pathFor('user.account'));
	}

	public function getPhoto($request, $response)
	{
		return $this->container->view->render($response, '/user/accountphoto.twig');
	}

	public function postPhoto($request, $response)
	{
		$userId = $_SESSION['user'];
		$ok = array("image/png", "image/jpeg", "image/jpg", "image/gif");
		$image = mb_strtolower($_FILES['image']['name']);
		$path = getcwd();
		$target = $path . "/uploads/users/" . $_SESSION['user'] . '/' . $image;
		if (Photo::addPhoto($userId, $image)) {
			if($rofl = move_uploaded_file($_FILES['image']['tmp_name'], $target)){
				$type = mime_content_type($target);
				if (in_array($type, $ok)){
					$this->container->flash->addMessage('info', 'Sucessfuly uploaded photo');
				} else {
					Photo::deletePhoto($image, $userId);
					$this->container->flash->addMessage('error', 'Wrong file format');
				}
			} else {
				echo "not " . $_FILES["file"]["error"];
			}
		} else {
			$this->container->flash->addMessage('error', 'You cant upload more than 5 photo');
		}
		return $response->withRedirect($this->container->router->pathFor('user.account'));
	}

	public function getToken($request, $response, $args)
	{
		$email = $args['email'];
		$token = $args['token'];
		$TrueToken = User::getToken($email);
		$TrueEmail = User::getMail($email);
		if($TrueEmail == $email && $TrueToken == $token){
			$result = User::activateUser($email);
			$this->container->flash->addMessage('info', 'Profile Activated');
		}
		else {
			$result = false;
			$this->container->flash->addMessage('error', 'Smthng Wrong!');
		}
		return $response->withRedirect($this->container->router->pathFor('home'));
	}

	public function getRecover($request, $response, $args)
	{
		$email = $args['email'];
		$token = $args['token'];
		$TrueToken = User::getToken($email);
		$TrueEmail = User::getMail($email);
		if($TrueEmail == $email && $TrueToken == $token){
			$this->container->flash->addMessage('info', 'You can change ur password');
			return $response->withRedirect($this->container->router->pathFor('user.recover',
			['email' => $email]));
		} else {
			$result = false;
			$this->container->flash->addMessage('error', 'Smthng Wrong!');
			return $response->withRedirect($this->container->router->pathFor('user.signin'));
		}
	}

	public function postRecover($request, $response, $args)
	{
		$data['email'] = $args['email'];
		$data['password'] = $request->getParam('password');
		$user = User::getUserByEmail($data['email']);
		$errors = $this->catchChange("recover", $data);
		if (count($errors) !== 0) {
			$this->container->view->getEnvironment()->addGlobal('errors', $errors);
			$this->container->view->getEnvironment()->addGlobal('email', $user['email']);
			return $this->container->view->render($response, '/user/recover.twig');
		} else {
			User::updatePassword($user['id'], $data['password']);
			$this->container->flash->addMessage('info', 'Your password succesfuly changed.');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}
	}

	private function catchErrors($data)
	{
		$errors = array();

		try {
			$email = $this->validate("email", $data['email']);
		} catch (Exception $e) {
			$errors['email'] = $e->getMessage();
		}
		try {
			$login = $this->validate("login", $data['login']);
		} catch (Exception $e) {
			$errors['login'] = $e->getMessage();
		}
		try {
			$password = $this->validate("password", $data['password']);
		} catch (Exception $e) {
			$errors['password'] = $e->getMessage();
		}
		return $errors;
	}

	private function catchChange($field, $data)
	{
		$errors = array();
		if ($field == "email") {
			try {
				$email_new = $this->validate("emailMatch", $data);
			} catch (Exception $e) {
				$errors['email_new'] = $e->getMessage();
			}
		} elseif ($field == "password") {
			try {
				$password_new = $this->validate("passwordMatch", $data);
			} catch (Exception $e) {
				$errors['password_new'] = $e->getMessage();
			}
		} else if ($field == "recover") {
			try {
				$password = $this->validate("password", $data['password']);
			} catch (Exception $e) {
				$errors['password'] = $e->getMessage();
			}
		}
		return $errors;
	}

	private function validate($field, $data)
	{
		switch ($field) {
			case "email":
				return User::emailValidation($data);
			case "login":
				return User::loginValidation($data);
			case "password":
				return User::passwordValidation($data);
			case "emailMatch":
				return User::emailChanging($data);
			case "passwordMatch":
				return User::passwordChanging($data);
		}
		return false;
	}
}
