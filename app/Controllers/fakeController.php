<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Fake;

class FakeController extends Controller
{

	public function index($request, $response)
	{
		$tmp = Fake::getAllInfo();
		$i = 0;
		foreach ($tmp as $value) {
			$tmp = User::getUserLoginByID($value['user_id']);
			$tm = User::getUserLoginByID($value['id']);
			$fakes[$i]['who'] = $tm['login'];
			$fakes[$i]['whom'] = $tmp['login'];
			$i++;
		}
		$this->container->view->getEnvironment()->addGlobal('fakes', $fakes);
		return $this->container->view->render($response, 'fakes.twig');
	}
}
