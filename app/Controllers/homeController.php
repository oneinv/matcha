<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;

class HomeController extends Controller
{
	public function index($request, $response)
	{
		$data = User::getAllUsersSorted();
		$this->container->view->getEnvironment()->addGlobal('data', $data);
		return $this->container->view->render($response, 'home.twig');
	}
}
