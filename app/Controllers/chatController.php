<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;

class ChatController extends Controller
{

	public function index($request, $response)
	{
		$user_login = User::getFullUserByID($_SESSION['user']);
		$this->container->view->getEnvironment()->addGlobal('user_login', $user_login['login']);
		return $this->container->view->render($response, 'chat.twig');
	}
}
