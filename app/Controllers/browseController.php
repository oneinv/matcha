<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Like;
use App\Models\Views;
use App\Models\Ignore;
use App\Models\Fake;
use App\Models\Online;
use App\Models\Photo;
use App\Auth\Auth;

class BrowseController extends Controller
{
	public function index($request, $response)
	{
	    if ($_SESSION['list'] == "please fill all your info in settings") {
	        $data = "please fill all your info in settings" ;
            $this->container->view->getEnvironment()->addGlobal('data', $data);
            return $this->container->view->render($response, '/main/find.twig');
        }
	    $users = $_SESSION['list'];

		$i = 0;
		foreach ($users as $user) {
			$data[$i]['id'] = $user['id'];
			$data[$i]['name'] = $user['name'];
			$data[$i]['login'] = $user['login'];
			$data[$i]['age'] = $user['age'];
			$data[$i]['avatar'] = $user['avatar'];
			$data[$i]['fame'] = $user['fame'];
			$data[$i]['geo'] = $user['geo'];
			$data[$i]['tags'] = $user['tags'];
			if ($user['avatar'] == "default.jpg")
				$data[$i]['avatar'] = "default.jpg";
			$data[$i]['avatar'] = $user['avatar'];
			$i++;
		}
		if (isset($_GET['skip']) && $_GET['skip'] != 0) {
			$user_id_skip = $_GET['skip'];
			foreach ($data as $key => $value) {
				if ($value['id'] === $user_id_skip) {
					unset($data[$key]);
				}
			}
			reset($data);
		}
		if (count($_SESSION['list']) == 0) {
			$this->container->flash->addMessage('info', 'No matches for you:(');
			$msg = "No matches for you ;()";
			$this->container->view->getEnvironment()->addGlobal('msg', $msg);
		}

		$data = User::sortBy($data, "fame");
		$this->container->view->getEnvironment()->addGlobal('data', $data);
		//rewrite SESSION match list
		$_SESSION['list'] = $data;
		return $this->container->view->render($response, '/main/find.twig');
	}

	public function getMainPage($request, $response, $args)
	{
		$user_data = User::getUserByLogin($args['login']);
		if ($user_data === false) {
			$this->container->flash->addMessage('info', 'No such user!');
			return $response->withRedirect($this->container->router->pathFor('home'));
		}
		$id = $_SESSION['user'];
		$user_id = User::getUserIdByLogin($args['login']);
		$photos = Photo::getPhotosByUserId($user_data['id']);
		if ($online = Online::getOnline($user_data['id']))
			;
		else
			$online['last'] = "user wasnt on site long time ago";
		if (!Views::isViewedByUser($user_data['id'], $id)){
			Views::addView($user_data['id'], $id);
			User::updateFame($user_data['id']);
			User::notificROFL($user_data['id'], "Someone viewed you profile");
		}
		$ignored = Ignore::isIgnoredByUser($user_data['id'], $id);
		$liked = Like::isLikedByUser($user_data['id'], $id);
		$reported = Fake::isFakedByUser($user_data['id'], $id);
		$this->container->view->getEnvironment()->addGlobal('id', $id);
		$this->container->view->getEnvironment()->addGlobal('user_id', $user_data['id']);
		$this->container->view->getEnvironment()->addGlobal('user_session', $_SESSION['user']);
		$this->container->view->getEnvironment()->addGlobal('photos', $photos);
		$this->container->view->getEnvironment()->addGlobal('online', $online);
		$this->container->view->getEnvironment()->addGlobal('user_data', $user_data);
		$this->container->view->getEnvironment()->addGlobal('liked', $liked);
		$this->container->view->getEnvironment()->addGlobal('ignored', $ignored);
		$this->container->view->getEnvironment()->addGlobal('reported', $reported);
		return $this->container->view->render($response, '/main/page.twig');
	}

	public function postMainPage($request, $response, $args)
	{
		$user_data = User::getUserByLogin($args['login']);
		$id = $_SESSION['user'];
		$doing = $request->getParams();
		if (isset($doing['like'])) {
			Like::addLike($user_data['id'], $id);
			User::notificROFL($user_data['id'], "Someone Liked you");
			User::updateFame($user_data['id']);
 			if (Like::isLikedByUser($id, $user_data['id'])) {
				User::notificROFL($user_data['id'], "you have match, go to chat");
				return $response->withRedirect($this->container->router->pathFor('chat'));
			}
		} else if (isset($doing['unlike'])) {
			Like::deleteLike($user_data['id'], $id);
			User::notificROFL($user_data['id'], "Someone UnLiked you");
			// ti geniy gospdodi kak tak??????????????????
			User::updateFame($user_data['id']);
		}
		return $response->withRedirect($this->container->router->pathFor('find.next',
		['login' => $args['login']]));
	}

	public function sortMain($request, $response, $args)
	{
		$list = $_SESSION['list'];
		$list = User::sortBy($list, $args['sort']);
		$this->container->view->getEnvironment()->addGlobal('data', $list);
		return $this->container->view->render($response, '/main/find.twig');
	}

	public function ignore($request, $response, $args)
	{
		$user_data = User::getUserByLogin($args['login']);
		$id = $_SESSION['user'];
		Ignore::addIgnore($user_data['id'], $id);
		Auth::updateList($_SESSION['user']);
		$this->container->flash->addMessage('info', 'User Ingored!');
		return $response->withRedirect($this->container->router->pathFor('home'));
	}

	public function fake($request, $response, $args)
	{
		$user_data = User::getUserByLogin($args['login']);
		$id = $_SESSION['user'];
		$fake = Fake::addFake($user_data['id'], $id);
		$this->container->flash->addMessage('info', 'User Reported!');
		return $response->withRedirect($this->container->router->pathFor('home'));
	}
}
