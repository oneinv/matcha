<?php

namespace App\Controllers;

use Slim\Views\Twig as View;
use App\Models\User;
use App\Models\Like;
use App\Models\Views;
use App\Models\Location;
use App\Models\Tags;

class SearchController extends Controller
{
	public function index($request, $response)
	{
        if ($_SESSION['list'] == "please fill all your info in settings") {
            $data = "please fill all your info in settings" ;
            $this->container->view->getEnvironment()->addGlobal('data', $data);
            return $this->container->view->render($response, '/main/find_search.twig');
        }
		$users = $_SESSION['list'];

		$i = 0;
		foreach ($users as $user) {
			$data[$i]['id'] = $user['id'];
			$data[$i]['login'] = $user['login'];
			$data[$i]['name'] = $user['name'];
			$data[$i]['age'] = $user['age'];
			$data[$i]['avatar'] = $user['avatar'];
			$data[$i]['fame'] = $user['fame'];
			$data[$i]['geo'] = $user['geo'];
			$data[$i]['tags'] = $user['tags'];
			$i++;
		}
		if (isset($_GET['skip']) && $_GET['skip'] != 0) {
			$user_skip['id'] = $_GET['skip'];
			$key = array_search($user_skip['id'], array_column($users, 'id'));
			array_splice($_SESSION['list'], $key, 1);
		}
		if ($_SESSION['list']) { // make it with flash msg
			$msg = "no matches for you ;(" ;
			$this->container->view->getEnvironment()->addGlobal('msg', $msg);
		}

		$this->container->view->getEnvironment()->addGlobal('data', $data);
		return $this->container->view->render($response, '/main/find_search.twig');
	}

	public function getSearch($request, $response, $args)
	{
        if ($_SESSION['list'] == "please fill all your info in settings") {
            $data = "please fill all your info in settings" ;
            $this->container->view->getEnvironment()->addGlobal('data', $data);
            return $this->container->view->render($response, '/main/search.twig');
        }
		$loc = Location::getLoc();
		$tags = Tags::getTags();
		$this->container->view->getEnvironment()->addGlobal('tags', $tags);
		$this->container->view->getEnvironment()->addGlobal('loc', $loc);
		return $this->container->view->render($response, '/main/search.twig');
	}

	public function postSearch($request, $response, $args)
	{
		$data  = $request->getParams();
		unset($data['csrf_name']);
		unset($data['csrf_value']);

		if (isset($data['tag_list'])) {
			$data['tag_list'] = implode(",",$data['tag_list']);
		} else {
			$data['tag_list'] = "";
		}
		if ($data['min_age'] == 0)
			$data['min_age'] = 18;
		if ($data['max_age'] == 0)
			$data['max_age'] = 99;
		if ($data['min_fame'] == 0)
			$data['min_fame'] = 0;
		if ($data['max_fame'] == 0)
			$data['max_fame'] = 999;
		$data['geo']= $data['city']. "/" .$data['country'];
		unset($data['city']);
		unset($data['country']);

		$my_tags = explode(',', $data['tag_list']);
		$_SESSION['user_tag_list'] = $my_tags;
		$list = User::searchUsers($data);
		$list_w_tags = User::tagsIntersection($my_tags, $list);
		foreach ($list_w_tags as $key => $value) {
			if ($value['count'] === 0)
				unset($list_w_tags[$key]);
		}
		reset($list_w_tags);
		$_SESSION['search_list'] = $list_w_tags;
		$this->container->view->getEnvironment()->addGlobal('data', $list_w_tags);
		return $this->container->view->render($response, '/main/find_search.twig');
	}

	public function sortSearch($request, $response, $args)
	{
		$list = $_SESSION['search_list'];
		$list = User::sortBy($list, $args['sort']);
		$this->container->view->getEnvironment()->addGlobal('data', $list);
		return $this->container->view->render($response, '/main/find_search.twig');
	}
}
