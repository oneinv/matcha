<?php

namespace App\Auth;

use App\Models\User;
use App\Models\Ignore;

class Auth
{
	public function user()
	{
		return User::getFullUserByID($_SESSION['user']);
	}

	public function check()
	{
		return isset($_SESSION['user']);
	}

	public function is_auth()
	{
		return User::checkConfirm($_SESSION['user']);
	}

	public function my_sort($users, $user_id)
	{
		$i = 0;
		foreach ($users as $user) {
		    if ($user['id'] == $user_id)
		        continue ;
			$tags[$i]['id'] = $user['id'];
			$tags[$i]['fame'] = $user['fame'];
			$tags[$i]['login'] = $user['login'];
			$tags[$i]['name'] = $user['name'];
			$tags[$i]['lastname'] = $user['lastname'];
			$tags[$i]['age'] = $user['age'];
			$tags[$i]['gender'] = $user['gender'];
			$tags[$i]['preference'] = $user['preference'];
			$tags[$i]['bio'] = $user['bio'];
			$tags[$i]['tags'] = $user['tags'];
			$tags[$i]['geo'] = $user['geo'];
			$tags[$i]['avatar'] = $user['avatar'];

			$i++;
		}

		$my_tags = User::getUserTags($_SESSION['user']);
		$my_tags = explode(',', $my_tags);
		$i = 0;
		foreach ($tags as $tag) {
			$tmp[$i]['id'] = $tag['id'];
			$tmp[$i]['tags'] = explode(',', $tag['tags']);
			$tmp[$i]['fame'] = $tag['fame'];
			$tmp[$i]['login'] = $tag['login'];
			$tmp[$i]['name'] = $tag['name'];
			$tmp[$i]['lastname'] = $tag['lastname'];
			$tmp[$i]['age'] = $tag['age'];
			$tmp[$i]['gender'] = $tag['gender'];
			$tmp[$i]['preference'] = $tag['preference'];
			$tmp[$i]['bio'] = $tag['bio'];
			$tmp[$i]['geo'] = $tag['geo'];
			$tmp[$i]['avatar'] = $tag['avatar'];
			$i++;
		}

		$i = 0;
		foreach ($tmp as $temp) {
			$test[$i]['count'] = count(array_intersect($my_tags, $temp['tags']));
			$test[$i]['id'] = $temp['id'];
			$test[$i]['fame'] = $temp['fame'];
			$test[$i]['login'] = $temp['login'];
			$test[$i]['name'] = $temp['name'];
			$test[$i]['lastname'] = $temp['lastname'];
			$test[$i]['age'] = $temp['age'];
			$test[$i]['gender'] = $temp['gender'];
			$test[$i]['preference'] = $temp['preference'];
			$test[$i]['bio'] = $temp['bio'];
			$test[$i]['tags'] = $temp['tags'];
			$test[$i]['geo'] = $temp['geo'];
			$test[$i]['avatar'] = $temp['avatar'];
			$i++;
		}
		$fame = array_column($test, 'fame');
		$count = array_column($test, 'count');
		array_multisort($fame, SORT_DESC, $count, SORT_DESC, $test);
		return $test;
	}

	public function attempt($email, $password)
	{
		if (!$user = User::checkMailExists($email))
			return false;
		$user = User::getUserbyEmail($email);
		$rofl = password_verify($password, $user['password']);
		if (password_verify($password, $user['password'])) {
			$_SESSION['user'] = $user['id'];
			$this->updateList($user['id']);
			return true;
		}
		return false;
	}

	public function updateList($user_id)
	{
		$user = User::getFullUserByID($user_id);
		$check = User::isReady($user_id);
		if ($check != "okay") {
			$_SESSION['list'] = "please fill all your info in settings";
		} else {
			$_SESSION['list'] = Auth::my_sort(User::getAllUsersSortedNew($user), $user['id']);
			foreach ($_SESSION['list'] as $key => $value) {
				if (Ignore::isIgnoredByUser($value['id'], $user['id']))
					unset($_SESSION['list'][$key]);
			}
			reset($_SESSION['list']);
		}
	}

	public function logout()
	{
		unset($_SESSION['user']);
	}
}
