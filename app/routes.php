<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use App\Middleware\ConfirmMiddleware;

$app->get('/', 'HomeController:index')->setName('home');

$app->group('', function () {
	$this->get('/user/signup', 'UserController:getSignUp')->setName('user.signup');
	$this->post('/user/signup', 'UserController:postSignUp');

	$this->get('/user/signin', 'UserController:getSignIn')->setName('user.signin');
	$this->post('/user/signin', 'UserController:postSignin');

	$this->get('/user/account/confirm/{email:
	[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}&{token:[a-zA-z0-9.-]+}',
	 			'UserController:getToken');
  	$this->get('/user/account/recover/{email:
	[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}&{token:[a-zA-z0-9.-]+}',
				'UserController:getRecover');
	$this->get('/user/account/recoverpass/{email:[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}',
	 			'UserController:postRecover')->setName('user.recover');

})->add(new GuestMiddleware($container));


$app->group('', function () {

	$this->get('/user/signout', 'UserController:getSignOut')->setName('user.signout');

	$this->get('/user/mail', 'UserController:getChangeEmail')->setName('user.mail');
	$this->post('/user/mail', 'UserController:postChangeEmail');

	$this->get('/user/password', 'UserController:getChangePassword')->setName('user.password');
	$this->post('/user/password', 'UserController:postChangePassword');



	$this->get('/user/account', 'UserController:getData')->setName('user.account');

	$this->get('/user/account/{id:[0-9]+}', 'UserController:getAva');
	$this->get('/user/account/del/{id}', 'UserController:delPhoto');

	$this->get('/user/accountchange', 'UserController:getChangeData')->setName('user.accountchange');
	$this->post('/user/accountchange', 'UserController:postChangeData');

	$this->get('/user/accountphoto', 'UserController:getPhoto')->setName('user.accountphoto');
	$this->post('/user/accountphoto', 'UserController:postPhoto');

	$this->get('/browse/main/{login}', 'BrowseController:getMainPage')
	->setName('find.next');
	$this->post('/browse/main/{login}', 'BrowseController:postMainPage');

	$this->get('/browse/sortBy/{sort}', 'BrowseController:sortMain')
	->setName('sort.by');

	$this->get('/browse/main/{login}/ignore', 'BrowseController:ignore')->setName('ignore');
	$this->get('/browse/main/{login}/report', 'BrowseController:fake')->setName('fake');

	$this->get('/browse/main', 'BrowseController:index')->setName('main.page');

	$this->get('/search/list', 'SearchController:getSearch')->setName('search.page');
	$this->post('/search/list', 'SearchController:postSearch');

	$this->get('/search/sortBy/{sort}', 'SearchController:sortSearch')
	->setName('sort_search.by');

	$this->get('/chat', 'ChatController:index')->setName('chat');
	$this->get('/notific', 'NotificController:index')->setName('notific');

	$this->get('/search', 'SearchController:index');
	$this->get('/fake', 'FakeController:index');

})->add(new AuthMiddleware($container))->add(new ConfirmMiddleware($container));
