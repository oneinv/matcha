<?php

namespace App\Models;

use App\Components\Db;
use PDO;

class Photo
{
	public static function addPhoto($userId, $image, $is_ava = false)
	{
		$db = Db::getConnection();

		$sq = "SELECT COUNT(*) FROM photo WHERE user_id =:user_id";
		$check = $db->prepare($sq);
		$check->bindParam(':user_id', $userId, PDO::PARAM_INT);
		$check->execute();
		$check1 = $check->fetch();
		if ($check1[0] >= 5) {
			return false;
		}
		$sql = "INSERT INTO photo(user_id, image, is_ava)
				VALUES (:user_id, :image, :is_ava)";
		$result = $db->prepare($sql);
        $result->bindParam(':image', $image, PDO::PARAM_STR);
		$result->bindParam(':is_ava', $is_ava, PDO::PARAM_BOOL);
		$result->bindParam(':user_id', $userId, PDO::PARAM_INT);
		return ($result->execute());
	}

	public static function setAva($userId)
	{
		$db = Db::getConnection();
		$sq = "SELECT image FROM photo WHERE user_id =:user_id AND is_ava = 1";
		$check = $db->prepare($sq);
		$check->bindParam(':user_id', $userId, PDO::PARAM_INT);
		$check->execute();
		$image = $check->fetch();
		if (!$image['image']) {
			return false;
		}
		$avatar = $image['image'];
		$sql = "UPDATE users SET avatar=:avatar WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':avatar', $avatar, PDO::PARAM_STR);
		$result->bindParam(':id', $userId, PDO::PARAM_INT);
		return ($result->execute());
	}

	public static function updatePhoto($id)
	{
		$db = Db::getConnection();
		$sql = "UPDATE photo SET image = :name WHERE id= :id";

		$name = $id . ".jpg";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
		return ($result->execute());
	}

	public static function addAvatar($userName, $name)
	{
		$db = Db::getConnection();
		$sql = "UPDATE user SET avatar = :name WHERE login=:userName";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':userName', $userName, PDO::PARAM_INT);
		return ($result->execute());
	}
	public static function getLastId()
	{
		$db = Db::getConnection();
		$sql = "SELECT id from photo";

		$result = $db->prepare($sql);
		$ids = array();
		$i = 0;
		$result->execute();
		while ($row = $result->fetch()) {
            $ids[$i]['id'] = $row['id'];
			$i++;
		}
		if($i == 0)
			return 0;
		else
			return $ids[$i - 1]['id'];
	}

	public static function getPhotoById($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT image FROM photo WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        return $row = $result->fetch();
    }

	public static function getLoginByPicture($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT login FROM photo WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$ret = $result->fetch();
		return $ret['login'];
	}

	public static function deletePhoto($image, $userId)
	{
		$db = Db::getConnection();
		$sql = "DELETE FROM photo WHERE image=:image AND user_id=:userId";
		$result = $db->prepare($sql);
		$result->bindParam(':userId', $userId, PDO::PARAM_INT);;
		$result->bindParam(':image', $image['image'], PDO::PARAM_STR);
		if ($result->execute()) {
            $path = "/public/uploads/users/".$userId."/".$image['image'];
		    if (unlink(ROOT . $path))
		        return true;
		    return false;
		}
		return false;
	}

	public static function deletePhotoById($id, $userId)
	{
		$db = Db::getConnection();
        $sql = "DELETE FROM photo WHERE id=:id AND user_id=:userId";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':userId', $userId, PDO::PARAM_INT);;
        unlink(ROOT."/public/uploads/users/$userId/$image");
		return ($result->execute());
	}

	public static function getPhotosByUserId($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT * from photo WHERE user_id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$i= 0;
		while ($row = $result->fetch()) {
            $ids[$i]['image'] = $row['image'];
			$ids[$i]['id'] = $row['id'];
			$i++;
		}
		return $ids;
	}

	public static function getPhotosByUserName($name)
	{
		$db = Db::getConnection();
		$sql = "SELECT * from photo WHERE name=:name";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->execute();
		while ($row = $result->fetch()) {
            $ids[$i]['image'] = $row['image'];
			$ids[$i]['login'] = $row['login'];
			$i++;
		}
		return $ids;
	}

	public static function updateAva($id, $user_id)
	{
		$db = Db::getConnection();
		$sql = "UPDATE photo SET is_ava = false WHERE user_id=:user_id";
		$result = $db->prepare($sql);
		$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$result->execute();
		$sql = "UPDATE photo SET is_ava = true WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		return ($result->execute());
	}

}
