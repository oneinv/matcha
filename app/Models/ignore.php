<?php


namespace App\Models;

use App\Components\Db;
use PDO;

class Ignore
{
		public static function addIgnore($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO ignores(id, user_id)
		 			VALUES (:id, :user_id)";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function isIgnoredByUser($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "SELECT COUNT(*) FROM ignores WHERE id=:id AND user_id=:user_id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->execute();
			$ignore = $result->fetch();
			return ($ignore[0]);
		}

		public static function deleteIgnore($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "DELETE FROM ignores WHERE id=:id AND user_id=:user_id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function getAllInfo()
		{
			$db = Db::getConnection();
			$sql = "SELECT count(*) FROM ignores";
			$result =$db->prepare($sql);
			$result->execute();
	 		$row = $result->fetch();
			return $row['0'];
		}

}
