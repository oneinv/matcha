<?php


namespace App\Models;

use App\Components\Db;
use PDO;

class Location
{
	public static function getLoc()
	{
		$db = Db::getConnection();
		$sql = "SELECT * FROM locations";
		$result = $db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $locs[$i] = $row;
			$i++;
		}
		return ($locs);
	}
		public static function getAllLikes($id)
		{
			$db = Db::getConnection();

			$sql = "SELECT COUNT(*)  FROM likes WHERE id = :id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$likes = $result->fetch();
			return ($likes[0]);
		}

		public static function addView($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO views(id, user_id, viewed)
		 			VALUES (:id, :user_id, :viewed)";
			$result = $db->prepare($sql);
			$hmm = 1;
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':viewed', $hmm, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function isViewedByUser($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "SELECT COUNT(*) FROM views WHERE id=:id AND user_id=:user_id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->execute();
			$liked = $result->fetch();
			return ($liked[0]);
		}

		public static function getAllInfo()
		{
			$db = Db::getConnection();
			$sql = "SELECT count(*) FROM likes";
			$result =$db->prepare($sql);
			$result->execute();
	 		$row = $result->fetch();
			return $row['0'];
		}

}
