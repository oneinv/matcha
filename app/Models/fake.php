<?php


namespace App\Models;

use App\Components\Db;
use PDO;

class Fake
{
		public static function addFake($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO fakes(id, user_id)
		 			VALUES (:id, :user_id)";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function isFakedByUser($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "SELECT COUNT(*) FROM fakes WHERE id=:id AND user_id=:user_id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->execute();
			$liked = $result->fetch();
			return ($liked[0]);
		}
		public static function getAllInfo()
		{
			$db = Db::getConnection();
			$sql = "SELECT * FROM fakes";
			$result =$db->prepare($sql);
			$result->execute();
	 		$row = $result->fetchAll(PDO::FETCH_ASSOC);
			return $row;
		}

}
