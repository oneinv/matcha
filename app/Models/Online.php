<?php


namespace App\Models;

use App\Components\Db;
use PDO;
use DateTime;

class Online
{
		public static function isAdded($id)
		{
			$db = Db::getConnection();
			$sql = "SELECT id FROM online WHERE id =:id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			return $result->fetch(PDO::FETCH_ASSOC);
		}

		public static function AddOnline($id)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO online(id)
					VALUES (:id)";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function UpdateOnline($id)
		{
			$datevar = date('Y-m-d H:i:s');
			$db = Db::getConnection();
			$sql = "UPDATE online
					SET last = :datevar
		 			WHERE id = :id";
			$result = $db->prepare($sql);
			$hmm = 1;
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':datevar', $datevar, PDO::PARAM_STR);
			return ($result->execute());
		}

		public static function getOnline($id)
		{
			$db = Db::getConnection();
			$sql = "SELECT last FROM online WHERE id =:id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			return $result->fetch(PDO::FETCH_ASSOC);
		}

}
