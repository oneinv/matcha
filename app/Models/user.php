<?php

namespace App\Models;

require_once(ROOT.'/app/PHPMailer/PHPMailer.php');
require_once(ROOT.'/app/PHPMailer/Exception.php');
require_once(ROOT.'/app/PHPMailer/SMTP.php');

use Pusher;
use PHPMailer\PHPMailer\PHPMailer;
use App\Components\Db;
use PDO;
use Exception;


class User
{
	public static function notificROFL($user_id, $msg)
	{
		// var_dump($user_id); die();

		  $options = array(
		    'cluster' => 'eu',
		    'useTLS' => true
		  );
		  $pusher = new Pusher\Pusher(
		    '5363577581d81372e311',
		    '697bc56f7842a8f2ce53',
		    '775066',
		    $options
		  );

		  $data['message'] = $msg;
		  // if ($user_id != $_SESSION['user'])
		  	$pusher->trigger($user_id, 'my-action', $data);
			// echo '<pre>';
			// var_dump($pusher->trigger('my-chanel', $user_id, $data));
			// echo '</pre>';
  	}

	public static function emailValidation($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL))
			;
		else
			throw new Exception('Wrong format for email!');
		if (Self::checkMailExists($email))
			throw new Exception('Email already exists!');
		return true;
	}

	public static function loginValidation($login)
	{
		if (strlen($login) >= 3 && ctype_alnum($login))
			;
		else
			throw new Exception('Wrong format for login!');
		if (Self::checkLoginExists($login))
			throw new Exception('Login already exists!');
		return true;
	}

	public static function passwordValidation($password)
	{
		if (strlen($password) >= 6 && ctype_alnum($password))
			return true;
		else
			throw new Exception('Wrong format for password!');
	}

	public static function emailChanging($data)
	{
		if ($data['email_old'] == $data['email'])
			;
		else
			throw new Exception('Email not match with current!');
		$hello =  Self::emailValidation($data['email_new']);
		if  (!$hello)
			throw new Exception($hello);
		else
			return true ;
	}

	public static function passwordChanging($data)
	{
		if (password_verify($data['password_old'], $data['password']))
			;
		else
			throw new Exception('Password not match with current!');
		$hello =  Self::passwordValidation($data['password_new']);
		if  (!$hello)
			throw new Exception($hello);
		else
			return true ;
	}

	public static function fill_db($str)
	{
		$db = Db::getConnection();
		$sql = $str;
		$result = $db->prepare($sql);
		$result->execute();
	}

	public static function register($login, $email, $password) {

		$db = Db::getConnection();
		$lastname = "";
		$gender = "";
		$age = 0;
		$preference = "";
		$bio = "";
		$tags = "";
		$age = "";
		$geo = "";
		$token = bin2hex(random_bytes(10));
		$avatar = "default.jpg";
		$sql = 'INSERT INTO users
		(login, name, lastname, age, email, password, gender, preference, bio, tags, avatar, geo, token, is_auth) '
		. 'VALUES (:login, :name, :lastname, :age, :email, :password, :gender, :preference, :bio, :tags, :avatar, :geo, :token, :is_auth)';
		$path = ROOT . '/upload/users/';
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
		$result->bindParam(':lastname', $lastname, PDO::PARAM_STR);
		$result->bindParam(':age', $age, PDO::PARAM_INT);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->bindParam(':password', $password, PDO::PARAM_STR);
		$result->bindParam(':gender', $gender, PDO::PARAM_STR);
		$result->bindParam(':preference', $preference, PDO::PARAM_STR);
		$result->bindParam(':bio', $bio, PDO::PARAM_STR);
		$result->bindParam(':tags', $tags, PDO::PARAM_STR);
		$result->bindParam(':avatar', $avatar, PDO::PARAM_STR);
		$result->bindParam(':geo', $geo, PDO::PARAM_STR);
		$result->bindParam(':token', $token, PDO::PARAM_STR);
		$result->bindParam(':is_auth', $a = 0, PDO::PARAM_INT);
		$result->execute();

		$id = self::getId($login);
		$path = ROOT . '/public/uploads/users/';
		if (!file_exists($path.$id))
			mkdir($path.$id);
	}

	public static function getId($login)
	{
		$db = Db::getConnection();
		$sql = 'SELECT id from users WHERE login =:login';
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		$res = $result->fetch(PDO::FETCH_ASSOC);
		return $res['id'];
	}

	public static function checkConfirm($id)
	{
		$db = Db::getConnection();
		$sql = 'SELECT is_auth FROM users WHERE id =:id';
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$ret = $result->fetch();
		return $ret['is_auth'];
	}

	public static function checkMailExists($email)
	{
		$db = Db::getConnection();

		$sql = 'SELECT COUNT(*) from users WHERE email = :email';
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();

		if($result->fetchColumn()){
			return true;
		}
		return false;
	}

	public static function checkLoginExists($login)
	{
		$db = Db::getConnection();

		$sql = 'SELECT COUNT(*) from users WHERE login = :login';
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		if($result->fetchColumn())
			return true;
		return false;
	}


	public static function checkLog()
	{
		if (isset($_SESSION['user'])) {
			return $_SESSION['user'];
		}
		// header("Location: /user/login");
	}

	public static function isGuest()
	{
		if (isset($_SESSION['user']))
			return false;
		return true;
	}

	public static function auth($name)
	{
		$_SESSION['user'] = $name;
	}

	public static function isReady($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT lastname, age, gender, preference, bio, tags, geo, avatar
                FROM users WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $res = $result->fetch(PDO::FETCH_ASSOC);
       	// var_dump($res);die();
        foreach ($res as $data) {
            if ($data || $data != 0)
                continue ;
            else
                return "please fill all informations about you in settings ( " . $data . " )";
        }
        return "okay";

    }
	public static function isAva($avatar, $id)
    {
        $db = Db::getConnection();
        $sql = "SELECT avatar FROM users WHERE avatar=:avatar AND id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':avatar', $avatar['image'], PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $res = $result->fetch(PDO::FETCH_ASSOC);
        if ($res) {
            return true ;
        }
        return false ;
    }

    public static function updateAvaToDefault($id)
    {
        $db = Db::getConnection();
        $sql = "UPDATE users SET avatar = '/uploads/default.jpg' WHERE id=:id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

	public static function updatePassword($id, $password)
	{
		$password = password_hash($password, PASSWORD_DEFAULT);
		$db = Db::getConnection();
		$sql = "UPDATE users
			SET password = :password
			WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':password', $password, PDO::PARAM_STR);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		return $result->execute();
	}

	public static function updateEmail($id, $email)
	{
		$db = Db::getConnection();
		$sql = "UPDATE users
			SET email = :email
			WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		return $result->execute();
	}

	public static function getUserIdByLogin($login)
	{
		// var_dump($login); die();
		$db = Db::getConnection();
		$sql = "SELECT id from users WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		$lol = $result->fetch(PDO::FETCH_ASSOC);
		// var_dump($lol); die();
		return $lol;
	}

	public static function getUserByID($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT name from users WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();

		$lol = $result->fetch(PDO::FETCH_ASSOC);
		return $lol;
	}
	public static function getUserLoginByID($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT login from users WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();

		$lol = $result->fetch(PDO::FETCH_ASSOC);
		return $lol;
	}

	public static function getFullUserByID($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT * from users WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		return $result->fetch(PDO::FETCH_ASSOC);
	}

	public static function getUserbyEmail($email)
	{
		$db = Db::getConnection();
		$sql = "SELECT * from users WHERE email=:email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();

		$lol = $result->fetch();
		return $lol;
	}

	public static function getUserByLogin($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT * from users WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		$login= $result->fetch();
		return $login;
	}



	public static function editData($id, array $data)
	{
		$db = Db::getConnection();
		$sql = "UPDATE users
			SET name = :name,
			lastname = :lastname,
			age = :age,
			gender=:gender,preference=:preference,bio=:bio,tags=:tag_list,geo=:geo
			WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->bindParam(':name', $data['name'], PDO::PARAM_STR);
		// $result->bindParam(':login', $data['login'], PDO::PARAM_STR);
		$result->bindParam(':lastname', $data['lastname'], PDO::PARAM_STR);
		$result->bindParam(':age', $data['age'], PDO::PARAM_INT);
		$result->bindParam(':gender', $data['gender'], PDO::PARAM_STR);
		$result->bindParam(':preference', $data['preference'], PDO::PARAM_STR);
		$result->bindParam(':bio', $data['bio'], PDO::PARAM_STR);
		$result->bindParam(':tag_list', $data['tag_list'], PDO::PARAM_STR);
		$result->bindParam(':geo', $data['geo'], PDO::PARAM_STR);
		// User::auth($login);
		return $result->execute();

	}

	public static function getToken($email)
	{
		$db = Db::getConnection();
		$sql = "SELECT token FROM users WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();
		$ret = $result->fetch();
		return $ret['token'];
	}


	public static function getMail($email)
	{
		$db = Db::getConnection();
		$sql = "SELECT email FROM users WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();
		$email = $result->fetch();
		return $email['email'];
	}


	public static function activateUser($email)
	{
		$db = Db::getConnection();
		$sql = "UPDATE users SET is_auth = 1 WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		return $result->execute();
	}

	public static function sendMail($email, $token = false, $doing)
	{
		$HTTP_HOST = $_SERVER['HTTP_HOST'];
		$user_mail = $email;
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host="smtp.gmail.com";
		$mail->SMTPDebug = 3;
		$mail->SMTPAuth= true;
		$mail->Username ="testingcamagru@gmail.com";
		$mail->Password = "ZZ002191";
		$mail->From="testingcamagru@gmail.com";
		$mail->FromName= "no-reply@camagru.com";
		$mail->addAddress($user_mail, "HELLO");
		$mail->isHTML(true);
		if ($doing == "activate") {
			$mail->Subject = "Please verify your account email";
			$mail->Body = "
		Please click on the link below to confirm your registration: <br><br>
		<a href='http://$HTTP_HOST/user/account/confirm/$user_mail&$token'>Click Here</a>";
		}
		else if ($doing =="recover") {
			$mail->Subject = "Recover password";
			$mail->Body = "
			Please click on the link below to Recover your password on matcha: <br><br>
			<a href='http://$HTTP_HOST/user/account/recover/$user_mail&$token'>Click Here</a>";
		}
		else if ($doing = "notific")
		{
			$mail->Subject = "Comment under your photo";
			$mail->Body = "Hello, Dear. One of your photo were commented. Come on Camagru and look:<br><br>
			<a href='http://$HTTP_HOST/photo'>Click Here</a>";
		}
		$mail->send();
	}

	public static function getAllUsersSorted()
	{
		$db = Db::getConnection();
		$sql = "SELECT * FROM users";
		$result =$db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $ids[$i] = $row;
			$i++;
		}
		return $ids;
	}

	public static function getAllUsers()
	{
		$db = Db::getConnection();
		$sql = "SELECT * FROM users";
		$result =$db->prepare($sql);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$ids[$i] = $row;
			$i++;
		}
		return $ids;
	}
	public static function getAllUsersSortedNew($user)
	{
		if ($user['preference'] == "both") {
			$geo = $user['geo'];
			$db = Db::getConnection();
			$sql = "SELECT * FROM users WHERE geo=:geo AND is_auth = 1";
			$result =$db->prepare($sql);
			$result->bindParam(':geo', $geo, PDO::PARAM_STR);
			$result->execute();
			$i = 0;
			while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
				$ids[$i]['id'] = $row['id'];
				$ids[$i]['fame'] = $row['fame'];
				$ids[$i]['login'] = $row['login'];
				$ids[$i]['name'] = $row['name'];
				$ids[$i]['lastname'] = $row['lastname'];
				$ids[$i]['age'] = $row['age'];
				$ids[$i]['gender'] = $row['gender'];
				$ids[$i]['preference'] = $row['preference'];
				$ids[$i]['bio'] = $row['bio'];
				$ids[$i]['tags'] = $row['tags'];
				$ids[$i]['geo'] = $row['geo'];
				$ids[$i]['avatar'] = $row['avatar'];
				$i++;
			}
			return $ids;
		}
		if ($user['preference'] == "female") {
			$gender = "female";
		} else if ($user['preference'] == "male") {
			$gender = "male";
		} else if ($user['preference'] == "transmale") {
			$gender = "transmale";
		} else if ($user['preference'] == "transfemale") {
			$gender = "transfemale";
		} else if ($user['preference'] == "random") {
			$sexual_game = array("male", "female", "transmale", "transfemale", "helisexual");
			$gender = $sexual_game[array_rand($sexual_game, 1)];
		}
		$geo = $user['geo'];
		$db = Db::getConnection();
		$sql = "SELECT * FROM users WHERE gender=:gender AND geo=:geo AND is_auth = 1";
		$result =$db->prepare($sql);
		$result->bindParam(':geo', $geo, PDO::PARAM_STR);
		$result->bindParam(':gender', $gender, PDO::PARAM_STR);
		$result->execute();
		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$ids[$i] = $row;
			$i++;
		}
		return $ids;
	}


	public static function getUserTags($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT tags FROM users WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$ret = $result->fetch(PDO::FETCH_ASSOC);
		$res['tags'] = $ret['tags'];
		return $res['tags'];
	}

	public static function updateFame($id)
	{
		$db = Db::getConnection();
		$sqlL = "SELECT COUNT(*) FROM likes WHERE id=:id";
		$like = $db->prepare($sqlL);
		$like->bindParam(':id', $id, PDO::PARAM_INT);
		$like->execute();
		$likes = $like->fetch(PDO::FETCH_ASSOC);
		$sqlV = "SELECT COUNT(*) FROM views WHERE id=:id";
		$view = $db->prepare($sqlV);
		$view->bindParam(':id', $id, PDO::PARAM_INT);
		$view->execute();
		$views = $view->fetch(PDO::FETCH_ASSOC);
		$fame = (($likes['COUNT(*)'] * 0.7) + ($views['COUNT(*)'] * 0.3)) / 2;
		$sql = "UPDATE users SET fame = :fame WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':fame', $fame, PDO::PARAM_STR);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
	}

    public static function searchUsers($data)
    {
        $min_age = $data['min_age'];
        $max_age = $data['max_age'];
        $min_fame = $data['min_fame'];
        $max_fame = $data['max_fame'];
        $geo = $data['geo'];
        $db = Db::getConnection();
        $sql = "SELECT * FROM users
				WHERE age BETWEEN $min_age AND $max_age
				AND fame BETWEEN $min_fame AND $max_fame
				AND geo =:geo";
        $result = $db->prepare($sql);
        $result->bindParam(':geo', $geo, PDO::PARAM_STR);
        $result->execute();
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $ids[$i] = $row;
            $i++;
        }
        return $ids;
    }

	public static function sortBy($data, $doing = "")
	{
		if ($doing == "")
			return $data;
		if ($doing == "age") {
			$age = array_column($data, 'age');
			array_multisort($age, SORT_DESC, $data);
			return $data;
		}
		if ($doing == "fame") {
			$fame = array_column($data, 'fame');
			array_multisort($fame, SORT_DESC, $data);
			return $data;
		}
		if ($doing == "tags") {
            $count = array_column($data, 'count');
            array_multisort($count, SORT_DESC, $data);
            return $data;
        }
		if ($doing == "location") {
			$geo = array_column($data, 'geo');
			array_multisort($geo, SORT_DESC, $data);
			return $data;
		}
			return $data;
	}

	public static function tagsIntersection($my_tags, $data)
	{
		foreach ($data as $tag) {
		    if ($tag['id'] == $_SESSION['user'])
		        continue ;
			$tmp[$i]['id'] = $tag['id'];
			$tmp[$i]['tags'] = explode(',', $tag['tags']);
			$tmp[$i]['fame'] = $tag['fame'];
			$tmp[$i]['login'] = $tag['login'];
			$tmp[$i]['name'] = $tag['name'];
			$tmp[$i]['lastname'] = $tag['lastname'];
			$tmp[$i]['age'] = $tag['age'];
			$tmp[$i]['gender'] = $tag['gender'];
			$tmp[$i]['preference'] = $tag['preference'];
			$tmp[$i]['bio'] = $tag['bio'];
			$tmp[$i]['geo'] = $tag['geo'];
			$tmp[$i]['avatar'] = $tag['avatar'];
			$i++;
		}
		$i = 0;
		foreach ($tmp as $temp) {
			$test[$i]['count'] = count(array_intersect($my_tags, $temp['tags']));
			$test[$i]['id'] = $temp['id'];
			$test[$i]['fame'] = $temp['fame'];
			$test[$i]['login'] = $temp['login'];
			$test[$i]['name'] = $temp['name'];
			$test[$i]['lastname'] = $temp['lastname'];
			$test[$i]['age'] = $temp['age'];
			$test[$i]['gender'] = $temp['gender'];
			$test[$i]['preference'] = $temp['preference'];
			$test[$i]['bio'] = $temp['bio'];
			$test[$i]['tags'] = $temp['tags'];
			$test[$i]['geo'] = $temp['geo'];
			$test[$i]['avatar'] = $temp['avatar'];
			$i++;
		}
		$count = array_column($test, 'count');
		array_multisort($count, SORT_DESC, $test);
		return $test;
	}
}
