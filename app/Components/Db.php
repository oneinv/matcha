<?php


namespace App\Components;
use PDO;

class Db
{
    public static function getConnection()
    {
        $paramsPath = ROOT . '/app/Components/db_params.php';
        $params = include($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
		try {
			$db = new PDO($dsn, $params['user'], $params['password']);
			$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        	$db->exec("set names utf8");
		} catch (PDOException $e) {
			$db = 'db failed:' . $e->getMessage();
		}
        return $db;
    }
}
