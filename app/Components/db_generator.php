<?php

function email_gen()
{
	$tlds = array("com", "net", "gov", "org");

	$char = "0123456789abcdefghijklmnopqrstuvwxyz";
	$ulen = mt_rand(3, 8);
    $dlen = mt_rand(2, 6);
	$a = "";
	for ($i = 1; $i <= $ulen; $i++) {
      $a .= substr($char, mt_rand(0, strlen($char)), 1);
    }
	$a .= "@";
	for ($i = 1; $i <= $dlen; $i++) {
      $a .= substr($char, mt_rand(0, strlen($char)), 1);
    }
	$a .= ".";
	$a .= $tlds[mt_rand(0, (sizeof($tlds)-1))];
	return ($a);
}

function grs($length = 10) {
    return substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
}

function gender()
{
	$sexual_game = array("male", "female", "transmale", "transfemale", "helisexual");
	$gender = $sexual_game[array_rand($sexual_game, 1)];
	return $gender;
}

function preference()
{
	$sexual_game = array("male", "female", "transmale", "transfemale", "random");
	$pref = $sexual_game[array_rand($sexual_game, 1)];
	return $pref;
}

function tags()
{
	$sexual_game = array("php", "love", "life", "tatoo", "sport");
	$pref = $sexual_game[array_rand($sexual_game, 1)];
	return $pref;
}

function generate_db()
{
	$str = "INSERT INTO `users` (`login`, `name`, `lastname`, `age`, `fame`, `email`, `password`, `gender`, `preference`, `bio`, `tags`, `geo`, `avatar`, `token`, `is_auth`)
	VALUES ";
	$arr = array();
	for ($i = 101; $i < 606; $i++)
	{
		$login = grs(5);
		$name = grs(6);
		$lastname = grs(6);
		$age = rand(18, 70);
		$fame = rand(0, 11);
		$email = email_gen();
		$pwd = bin2hex($login);
		$gender = gender();
		$pref = preference();
		$bio = grs(10);
		$tags = tags();
		$geo = "Kyiv/Ukraine";
		$avatar = "default.jpg";
		$token = grs(8);
		$is_auth = 1;
		$arr[$i] = $str."("."'".$login."',"
					."'".$name."','"
					.$lastname."','"
					.$age."','"
					.$fame."','"
					.$email."','"
					.$pwd."','"
					.$gender."','"
					.$pref."','"
					.$bio."','"
					.$tags."','"
					.$geo."','"
					.$avatar."','"
					.$token."','"
					.$is_auth."'"  .")";
	}
	return $arr;
}
